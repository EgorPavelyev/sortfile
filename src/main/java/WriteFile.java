import java.io.*;

public class WriteFile {
    public static void main(String[] args) {
        try {

            File newfile = new File("Text3.txt");
            FileOutputStream fos = new FileOutputStream(newfile, true);
            OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
            BufferedWriter bw = new BufferedWriter(osw);

            bw.write("1\n");
            bw.write("2\n");
            bw.write("3\n");
            bw.write("4\n");
            bw.write("5\n");
            bw.write("6\n");

            bw.close();
            osw.close();
            fos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}


